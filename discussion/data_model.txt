COURSE BOOKING APPLICATION
DATA MODELS

=========users============
{
	_id: ObjectID,
	firstName: String,
	lastName: String,
	age: String,
	gender: String,
	email: String,
	password: String,
	mobileNo: String,
	isAdmin: Boolean,
	enrollments: [
		{
			courseID: courseID,
			status: String,
			enrolledOn: Date
		}
	],
	paymentStatus: transactionID
}

========courses===========
{
	_id: ObjectID,
	courseName: String,
	description: String,
	price: Number,
	isActive: Boolean,
	createdOn: Date,
	slots: Number,
	enrollees: [
		{
			students: String,
			enrolledOn: Date
		}
	]
}


======transaction=========
{
	userID: String,
	courseID: String,
	isPaid: Boolean,
	totalAmount: Number,
	paymentMethod: String,
	datetimeCreated: Date
}

<!-- 
	Data Model design - the key consideration for the structure of your documents.

	Two types of design:

	1. Embedded data model
	2. Normalized/referenced data model   
-->

<!-- Embedded data model -->

{
	name: "ABC Company",
	contact: 09171234567,
	branches: [
		{
			name: "Branch One",
			address: "Pasig",
			contact: 09161234567
		}
	]
}
{
	userName: "thebinsubtleissubtle",
	contact: {
		mobileNum: 09171234567,
		phoneNum: 021234567,
		email: me@something.com,
		faxNo: 09351234567
	}

}

<!-- Normalized / Referenced Data Model -->

<!-- Branch Model -->
{
	_id: String,
	name: String,
	address: String,
	city: String,
	supplier_id: <supplier_id>
}

<!-- Suppliers Model -->
{
	_id: String,
	name: String,
	contact: String,
	branch: [
		<branch_id>
	]
}

